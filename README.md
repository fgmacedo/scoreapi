# Score API #

### POST /pontos

Querystring params: `nome`, `pontos`


### GET /ranking

Lista ordenada dos 5 primeiros lugares em format `nome;pontos`

### POST /reset

Reinicia o banco de dados.


# Rodar

`python main.py`