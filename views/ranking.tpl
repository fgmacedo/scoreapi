<?xml version="1.0" encoding="UTF-8"?>
<ranking>
% for idx, pessoa in enumerate(ranking):
    <pessoa>
        <posicao>{{idx+1}}</posicao>
        <nome>{{pessoa['nome']}}</nome>
        <pontos>{{pessoa['pontos']}}</pontos>
    </pessoa>
% end
</ranking>
