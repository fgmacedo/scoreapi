import bottle
import bottle.ext.sqlite
from bottle import template, request, response

app = bottle.Bottle()
plugin = bottle.ext.sqlite.Plugin(dbfile='scores.db')
app.install(plugin)

bottle.debug(True)


@app.route('/pontos')
def pontos(db):
    nome = request.query.nome
    pontos = int(request.query.pontos)
    if not nome or not pontos:
        return "resultado=0"

    row = db.execute("select id, pontos from scores where nome = ?", [nome]).fetchone()
    if row:
        if pontos > row['pontos']:
            db.execute("update scores set pontos = ? where id = ?", [pontos, row['id']])
    else:
        db.execute("insert into scores (nome, pontos) values (?, ?)", [nome, pontos])

    return "resultado=1"


@app.route('/ranking')
def ranking(db):
    response.headers['Content-Type'] = 'application/xml'
    ranking = db.execute("select nome, pontos from scores order by pontos desc limit 5").fetchall()
    return template('ranking', ranking=ranking)


@app.route('/reset', method='POST')
def do_reset(db):
    db.execute("delete from scores;")
    db.execute("delete from sqlite_sequence where name='scores';")
    return "resultado=1"

app.run(host='localhost', port=8080, reloader=True)
